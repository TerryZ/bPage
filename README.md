# bPage
基于jQuery、Bootstrap2、3进行开发，支持页面跳转、异步页面、异步数据等多模式的独立分页插件

## 状态

[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://mit-license.org/)

插件预览效果（分页栏）  
![bPage](https://terryz.github.io/image/bPage.png)

## 主要特性

<ul>
    <li>jQuery插件</li>
    <li>支持Bootstrap2、3的UI环境</li>
    <li>带有页面跳转、异步页面（服务端返回页面内容模式）、异步数据（服务端返回JSON数据）三种分页模式，可对业务场景进行灵活处理</li>
    <li>快速使用皮肤</li>
    <li>浏览器支持IE8+,chrome,firefox</li>
</ul>

## 使用入门、文档、实例
请访问 [https://terryz.github.io/bpage/index.html](https://terryz.github.io/bpage/index.html)